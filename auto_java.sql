-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 21 Cze 2019, 09:49
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Baza danych: `auto_java`
--
CREATE DATABASE IF NOT EXISTS `auto_java` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `auto_java`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `marki`
--

DROP TABLE IF EXISTS `marki`;
CREATE TABLE `marki` (
  `id_marka` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pojazdy`
--

DROP TABLE IF EXISTS `pojazdy`;
CREATE TABLE `pojazdy` (
  `id_pojazd` bigint(20) UNSIGNED NOT NULL,
  `id_marka` bigint(20) UNSIGNED NOT NULL,
  `id_silnik` bigint(20) UNSIGNED NOT NULL,
  `model` varchar(40) NOT NULL,
  `typ_nadwozia` tinyint(1) NOT NULL,
  `spalanie_miejscie` float NOT NULL,
  `spalanie_trasa` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `silniki`
--

DROP TABLE IF EXISTS `silniki`;
CREATE TABLE `silniki` (
  `id_silnik` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `moc` varchar(5) NOT NULL,
  `paliwo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `marki`
--
ALTER TABLE `marki`
  ADD UNIQUE KEY `id_marka` (`id_marka`);

--
-- Indeksy dla tabeli `pojazdy`
--
ALTER TABLE `pojazdy`
  ADD UNIQUE KEY `id_pojazd` (`id_pojazd`),
  ADD KEY `id_marka` (`id_marka`),
  ADD KEY `id_silnik` (`id_silnik`);

--
-- Indeksy dla tabeli `silniki`
--
ALTER TABLE `silniki`
  ADD UNIQUE KEY `id_silnik` (`id_silnik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `marki`
--
ALTER TABLE `marki`
  MODIFY `id_marka` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pojazdy`
--
ALTER TABLE `pojazdy`
  MODIFY `id_pojazd` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `silniki`
--
ALTER TABLE `silniki`
  MODIFY `id_silnik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `pojazdy`
--
ALTER TABLE `pojazdy`
  ADD CONSTRAINT `pojazdy_ibfk_1` FOREIGN KEY (`id_marka`) REFERENCES `marki` (`id_marka`),
  ADD CONSTRAINT `pojazdy_ibfk_2` FOREIGN KEY (`id_silnik`) REFERENCES `silniki` (`id_silnik`);
COMMIT;
