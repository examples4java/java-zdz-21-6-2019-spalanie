package Pojazdy;

public class Samochod {
	private String marka,model;
	private int nadwozie;
	private int silnik;
	//pola powinny być prywatne; trzeba dołożyć odpowiednie funkcje i poprawić pobieranie wartości tych pól w klasie Main
	public float miejski,trasa;
	
	public void setMarka(String marka) {this.marka=marka;}
	public void setmodel(String model) {this.model=model;}
	public void setSilnik(int silnik) {this.silnik=silnik;}
	public void setNadwozie(int nadwozie) {this.nadwozie=nadwozie;}
	public void setNadwozie(Nadwozie nadwozie) {this.nadwozie=nadwozie.get();}
	
	public void setSpalanie(float a, float b) {miejski=a; trasa=b;}
	//może dobrym pomysłem byłoby dodanie funkcji wprowadzającej spalanie miejskie i pozamiejskie osobno?
	
	public String getMarka() {return this.marka;}
	public String getmodel() {return this.model;}
	public int getSilnik() {return this.silnik;}
	public Nadwozie getNadwozie() {
		if (nadwozie<Nadwozie.values().length)	
			return Nadwozie.values()[nadwozie];
		return Nadwozie.CABRIO;
	}
	
	public static enum Nadwozie {
		COUPE(0),
		HATCHBACK(1),
		PICKUP(2),
		CABRIO(3),
		VAN(4);
		
		private final int v;
		
		private Nadwozie(int v) {this.v=v;}
		
		public int get() {return v;}
	}
}
