package Pojazdy;

public class Silnik {
	private String nazwa, moc;
	private int paliwo;
	
	public void setNazwa(String nazwa) {this.nazwa=nazwa;}
	public void setMoc(String moc) {this.moc=moc;}
	public void setPaliwo(int paliwo) {this.paliwo=paliwo;}
	public void setPaliwo(Paliwo paliwo) {this.paliwo=paliwo.get();}
	
	public String getNazwa() {return nazwa;}
	public String getMoc() {return moc;}
	public Paliwo getPaliwo() {
		return Paliwo.values()[paliwo];
	}
	
	public static enum Paliwo {
		BENZYNA(0),
		DIESEL(1);
		
		private final int v;
		
		private Paliwo(int v) {this.v=v;}
		
		public int get() {return v;}
	}
}
