/*
 * Co należy wykonać:
 * 
 * - podłączyć bibliotekę tool.jar (by program funkcjonował prawidłowo)
 * - uzupełnić funkcję calc o obliczanie ceny przejechanej trasy wskazanym samochodem z określonym paliwem (teraz liczy na sztywno)
 * - dodać zapis dodanych samochodów i silników do plików
 * - dodać odczyt zapisanych samochodów i silników z plików
 * - podłączyć zapis i odczyt z bazą danych
 * - dodać możliwość edycji parametrów samochodów i silników
 * - dodać do menu możliwość wyświetlania zapisanych samochodów oraz silników (obecnie brak)
 * - ładnie sformatować wyświetlanie menu i wyświetlanych wyników (obecnie menu wyświetla opcje wyboru obok siebie, mało czytelnie wygląda wyświetlanie 
 *   wyników dodawania samochodów czy też wyświetlanie wartości spalania)
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

import Pojazdy.Samochod;
import Pojazdy.Samochod.Nadwozie;
import Pojazdy.Silnik.Paliwo;
import Pojazdy.Silnik;
import Tools.Narzedzia;

public class Main {
	private static float cenaBenzyna=5.05f;
	private static float cenaDiesel=5.00f;
	
	private static List<Samochod> auta;
	private static List<Silnik> silniki;
	
	public static void main(String[] args) {
		Narzedzia tool = new Narzedzia();
		auta = new Vector<Samochod>();
		silniki = new Vector<Silnik>();
		if (args.length>0) {
			for(int i=0;i<args.length;i++) {
				if (args[i].contains("benz") && (i+1) < args.length)
					try {
						cenaBenzyna=Float.valueOf(args[++i]);
					}
					catch(Exception e) {
						System.err.println("Nie udało się zmienić ceny benzyny, spróbuj z menu programu!");
					}
				if ((args[i].contains("dies") || args[i].contains("rop")) && (i+1) < args.length)
					try {
						cenaDiesel=Float.valueOf(args[++i]);
					}
					catch(Exception e) {
						System.err.println("Nie udało się zmienić ceny diesel, spróbuj z menu programu!");
					}
			}
		}
		
		while(menu(tool));
		
		
	}
	private static boolean menu(Narzedzia t) {
		t.nPrint("Progam do obliczania spalania. Wybierz jedną z opcji:");
		t.nPrint("1. Dodanie samochodu");
		t.nPrint("2.Dodanie silnika");
		t.nPrint("3. Zmiana ceny benzyny");
		t.nPrint("4. Zmiana ceny ropy");
		t.nPrint("5. Zapis danych do bazy danych");
		t.nPrint("6.Odczyt danych z bazy");
		t.nPrint("7. Obliczenie spalania");
		t.nPrint("8. Zakończenie programu");
		int c = t.readInt();
		//tutaj brakuje dodatkowych opcji, tak samo jak zapisu/odczytu do pliku
		switch (c) {
		case 1: addCar(); break;
		case 2: addEngine(); break;
		case 5: saveToBase(); break;
		case 7: calc();  break;
			default: return false;
		}
		return true;
	}
	
	private static void calc() {
		//podanie samochodu
		
		Narzedzia t = new Narzedzia();
		t.nPrint("Proszę podać ile ile kilemetrów zostało przejechane: ");
		float km = t.readFloat();
		t.nPrint("Proszę podać ID samochodu, którym przejechano podany dystans: ");
		t.nPrint("ID\t\t|\tMarka\t\t\t\t\t|\tModel");
		t.nPrint("----------------|-----------------------------------------------|---------------------");
		for (int i=0;i<auta.size();i++) {
			t.nPrint(i +"\t\t|\t"+auta.get(i).getMarka()+"\t\t\t\t\t|\t"+auta.get(i).getmodel());
		}
		int autoModel=0;
		t.nPrint("Twój wybór to: ");
		autoModel = t.readInt();
		Samochod s = auta.get(autoModel);
		float sr = (s.getMiejskie() + s.getTrasa())/2;
		//wynik powinien wyświetlać liczby z precyzją do dwóch miejsc po przecinku, wyświetlać walutę (PLN lub zł)
		//brak wyliczeń gdyby auto było na ropę!
		t.nPrint("Kwota: " + (sr*(silniki.get(s.getSilnik()).getPaliwo()==Paliwo.BENZYNA ? cenaBenzyna : cenaDiesel)*(km/100)));
	}
	
	private static void addCar() {
		if (silniki.isEmpty()) {
			System.err.println("Lista silników jest pusta! Dodaj silnik!");
			return;
		}
		Narzedzia t = new Narzedzia();
		Samochod tmp = new Samochod();
		t.nPrint("Podaj markę: ");
		tmp.setMarka(t.readLine());
		
		t.nPrint("Podaj model: ");
		tmp.setmodel(t.readLine());
		
		t.nPrint("Podaj nadwozie: ");
		t.nPrint("(dostępne: ");
		for(int i=0;i<Nadwozie.values().length;i++)
			t.print(i + "-" + Nadwozie.values()[i].toString() + ", ");
		t.print(")");		
		tmp.setNadwozie(t.readInt());
		
		t.nPrint("Podaj silnik z następujących: ");
		int id = 0;
		for (Silnik stmp : silniki) {
			t.nPrint((id++) + " " + stmp.getNazwa());
		}
		tmp.setSilnik(t.readInt());
		
		t.nPrint("Podaj spalanie (miasto, trasa): ");
		//ta funkcja działą poprawnie, jednak dodawanie wartości jest dość problematyczne (wywołuje błąd konwersji liczb)
		//proszę to poprawić
		tmp.setSpalanie(t.readFloat(), t.readFloat());
		
		auta.add(tmp);
	}
	
	private static void addEngine() {
		Narzedzia t = new Narzedzia();
		Silnik tmp = new Silnik();
		t.nPrint("Podaj nazwę: ");
		tmp.setNazwa(t.readLine());
		
		t.nPrint("Podaj moc: ");
		tmp.setMoc(t.readLine());
		
		t.nPrint("Podaj paliwo: ");
		tmp.setPaliwo(t.readInt());
		
		silniki.add(tmp);
	}
	
	private static void showCars() {
		Narzedzia t = new Narzedzia();
		for (Samochod s : auta) {
			t.nPrint(s.getMarka() + " " + s.getmodel() + " " + s.getNadwozie() + " " + silniki.get(s.getSilnik()).getMoc());
		}
	}
	
	private static void saveToBase() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/auto_java?useUnicode=true&characterEncoding=utf-8","root","");
			Statement st = connection.createStatement();
			st.execute("DELETE FROM `silniki`;" );//+ 
			st.execute("ALTER TABLE `silniki` AUTO_INCREMENT = 1;");
			for (Silnik s : silniki) {
				st.execute("INSERT INTO `silniki` (`nazwa`,`moc`,`paliwo`) VALUES ('"+s.getNazwa()+"','"+ s.getMoc() +"',"+ s.getPaliwo().get() +");");
			}
			st.execute("DELETE FROM `pojazdy`;");
			st.execute("ALTER TABLE `pojazdy` AUTO_INCREMENT = 1;");
			st.execute("DELETE FROM `marki`;");
			st.execute("ALTER TABLE `marki` AUTO_INCREMENT = 1; ");
			for (Samochod s : auta) {
				int addMarka = 0;
				ResultSet rs = st.executeQuery("SELECT `id_marka` FROM `marki` WHERE `nazwa`='"+s.getMarka()+"';");
				if (rs.next()) {
					addMarka=rs.getInt(1);
				}
				else {
					st.execute("INSERT INTO `marki` (`nazwa`) VALUES ('"+s.getMarka()+"');");
					rs = st.executeQuery("SELECT `id_marka` FROM `marki` WHERE `nazwa`='"+s.getMarka()+"';");
					rs.next();
					addMarka=rs.getInt(1);
				}
				st.execute("INSERT INTO `pojazdy` (`id_marka`,`id_silnik`,`model`,`typ_nadwozia`,`spalanie_miejscie`,`spalanie_trasa`) VALUES ("+addMarka+","+s.getSilnik()+",'"+s.getmodel()+"',"+
				s.getNadwozie().get()+","+s.getMiejskie()+","+s.getTrasa()+");");
				
//				while (rs.next()) {
//					//if ()
////					System.out.println("Kolumna z nazwą zawiera: "+ rs.getString(2) +
////							", natomiast opis to: " + rs.getString(3));
//					//type type = (type) .nextElement();
//					
//				}
				//st.execute("")
			}
			connection.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
